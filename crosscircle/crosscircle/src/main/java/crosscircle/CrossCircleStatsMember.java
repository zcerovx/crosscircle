package crosscircle;


public class CrossCircleStatsMember {
	
	private String 		 name;
	private int 		 wins;
	private int 		 losses;
	private int 		 draws;

	
	public CrossCircleStatsMember(String name, int wins, int losses, int draws) {
        this.name = name;
        this.wins = wins;
        this.losses = losses;
        this.draws = draws;
    }
	
    public String getName() {
        return name;
    }
    
    public int getWins() {
        return wins;
    }
    
    public int getLosses() {
        return losses;
    }
    
    public int getDraws() {
        return draws;
    }
    
    public void incWins() {
    	wins++;
    }
    
    public void incLosses() {
    	losses++;
    }
    
    public void incDraws() {
    	draws++;
    }
}
