package crosscircle;

import java.util.LinkedList;
import java.util.Queue;

public class CrossCircleGameStatusFinished extends CrossCircleGameStatus {
	
	private int 	 	 gameId;
	private String 		 status;
	private String 		 winner;
	private Queue<CrossCircleGame> game = new LinkedList<CrossCircleGame>();
	
	public CrossCircleGameStatusFinished(int gameId, String status, String winner, Queue<CrossCircleGame> game) {
		this.gameId = gameId;
        this.status = status;
        this.winner = winner;
        this.game = game;
    }
	
	public int getGameId() {
        return gameId;
    }

    public String getStatus() {
        return status;
    }
    
    public String getWinner() {
        return winner;
    }
    
    public Queue<CrossCircleGame> getGame() {
        return game;
    }
}
