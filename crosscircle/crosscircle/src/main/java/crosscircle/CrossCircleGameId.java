package crosscircle;

public class CrossCircleGameId {

	private int gameId;
	
	public CrossCircleGameId(final int gameId) {
		this.gameId = gameId;
    }
	
	public int getGameId() {
        return gameId;
    }
}

