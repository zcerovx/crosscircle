package crosscircle;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Application {
	
	public static Map<Integer, CrossCirclePlayGame> gamesAll = new HashMap<Integer, CrossCirclePlayGame>();
	public static CrossCircleGameStats gameStatsAll = new CrossCircleGameStats();


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    public static Map<Integer, CrossCirclePlayGame> getGamesAll() {
    	return gamesAll;
    }
    
    public static CrossCircleGameStats getGameStatsAll() {
    	return gameStatsAll;
    }
}
