package crosscircle;

import java.util.ListIterator;
import java.util.Queue;
import java.util.Random;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CrossCircleGameController {

	
    @RequestMapping("game/new")
    public CrossCircleGameId gameId(@RequestParam(value="first", defaultValue="computer") String first, 
    		@RequestParam(value="second", defaultValue="computer") String second) {
    	
    	Random rg = new Random();
    	final int gameId = rg.nextInt();
    	String player = null;
    	
    	if(first != null && !first.equals("computer")) {
    		player = first;
    	} else if(second != null && !second.equals("computer")) {
    		player = second;
    	}
    	
    	if(player != null) {
    		Queue<CrossCircleStatsMember> gameStats = Application.getGameStatsAll().getStats();
    		ListIterator<CrossCircleStatsMember> keyIter = (ListIterator<CrossCircleStatsMember>) gameStats.iterator();
    		
    		boolean found = false;
    		while(keyIter.hasNext()) {
    			if(keyIter.next().getName().equals(first)) {
    				found = true;
    				break;
    			}
    		}
    		
    		if(!found) {
    			Application.getGameStatsAll().getStats().add(new CrossCircleStatsMember(player, 0, 0, 0));
    		}
    		Application.getGamesAll().put(gameId, new CrossCirclePlayGame(gameId, player, "computer", "inProgress"));
    		
    		return new CrossCircleGameId(gameId);
    	}
    	
        return null;
    }
    
    @RequestMapping("game/status")
    public CrossCircleGameStatus gameStatus(@RequestParam(value="gameId") int gameId) {
    	
    	CrossCirclePlayGame playGame = Application.getGamesAll().get(gameId);
    	if(playGame != null && playGame.getStatus().equals("inProgress")) {
    		return new CrossCircleGameStatusInProg(gameId, playGame.getStatus(), playGame.getGame());
    	} else if(playGame != null && playGame.getStatus().equals("finished")) {
    		return new CrossCircleGameStatusFinished(gameId, playGame.getStatus(), playGame.getWinner(), playGame.getGame());
    	} 
    	
    	return null;
    }
    
    @RequestMapping("game/play")
    public HttpStatus play(@RequestParam(value="gameId") int gameId, @RequestParam(value="row_number") int row_number, 
    		@RequestParam(value="column_number") int column_number) {
        
    	CrossCirclePlayGame playGame = Application.getGamesAll().get(gameId);
    	if(!playGame.play(row_number, column_number)) {
    		return HttpStatus.PRECONDITION_FAILED;
    	} 
    	return HttpStatus.ACCEPTED;
    }
    
    @RequestMapping("game/stats")
    public CrossCircleGameStats stats() {
        
    	CrossCircleGameStats gameStats = Application.getGameStatsAll();
    	if(gameStats != null) {
    		return gameStats;
    	}
    	return null;
    }
}