package crosscircle;

import java.util.LinkedList;
import java.util.Queue;

public class CrossCircleGameStatusInProg extends CrossCircleGameStatus {
	
	private int 	 	 gameId;
	private String 		 status;
	private Queue<CrossCircleGame> game = new LinkedList<CrossCircleGame>();
	
	public CrossCircleGameStatusInProg(int gameId, String status, Queue<CrossCircleGame> game) {
		this.gameId = gameId;
        this.status = status;
        this.game = game;
    }
	
	public int getGameId() {
        return gameId;
    }

    public String getStatus() {
        return status;
    }
    
    public Queue<CrossCircleGame> getGame() {
        return game;
    }

}
